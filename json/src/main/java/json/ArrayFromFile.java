package json;

import java.io.FileReader;

import com.google.gson.Gson;

public class ArrayFromFile {
	public static void main (String[] args) throws Exception
    {
		/*
        URL objectGet = new URL("http://www.jsoneditoronline.org/?id=0110bf9767374ce8e018d8a52f53fdbe");

        URLConnection uc = objectGet.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));

        Book[] books = new Gson().fromJson(in, Book[].class);
        System.out.println(new Gson().toJson(books));
        */
		
		Book[] books = new Gson().fromJson(new FileReader("Q:/sophie/programming/4CHIF - PS TT/sts workspace/json-start/src/main/java/json-books.json"), Book[].class);
	    System.out.println(new Gson().toJson(books));
    }   

}
